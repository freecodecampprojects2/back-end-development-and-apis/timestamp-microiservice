const error = "Invalid Date";
function timeStampService(date) {
  /**
   * date * 1 tries to cast string to number
   * or make unixTimestamp value equal to "Invalid Date"
   */
  const unixTimestamp = new Date(date * 1);
  /**
   * tries to create a new date object from string
   * or make its value equal to "Invalid Date"
   */
  const stringTimestamp = new Date(date);
  const time =
    unixTimestamp == "Invalid Date" ? stringTimestamp : unixTimestamp;
  return time == "Invalid Date" ? { error: "Invalid Date" } : time;
}

module.exports = timeStampService;
